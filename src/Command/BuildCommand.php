<?php
// (c) Copyright by authors of the Tiki Wiki CMS Groupware Project
//
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.
// $Id$

namespace TikiPackages\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;
use TikiPackages\GeneratePages;
use TikiPackages\PackPackage;

class BuildCommand extends Command
{
	const BASE_REPO_TEMPLATE = 'https://gitlab.com/tikiwiki/tiki/-/raw/%s';
	const PACKAGES_FOLDER = 'packages';

	protected function configure()
	{
		$this
			->setName('build')
			->setDescription('Build Packages')
			->addArgument(
				'version',
				InputArgument::REQUIRED | InputArgument::IS_ARRAY,
				'List of versions to process'
			)
			->addOption(
				'public',
				'p',
				InputOption::VALUE_REQUIRED,
				'Public folder',
				'public'
			)
			->addOption(
				'tmp',
				't',
				InputOption::VALUE_REQUIRED,
				'Temporary folder',
				'.tmp'
			);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$publicFolder = $input->getOption('public');
		$tempFolder = $input->getOption('tmp');
		$versions = $input->getArgument('version');

		if (! is_dir($tempFolder)) {
			mkdir($tempFolder, 0755, true);
		}

		$versionList = [];
		foreach ($versions as $version) {
			$path = $version = trim($version);
			if ($path == 'trunk' || $path == 'master') {
				$version = 'trunk';
				$path = 'master';
			}
			$versionList[$version] = [
				'version' => $version,
				'path' => $path,
				'packages' => [],
			];
		}

		$packagePacker = new PackPackage();
		$packagesFolder = $publicFolder . DIRECTORY_SEPARATOR . self::PACKAGES_FOLDER;

		if (! is_dir($packagesFolder)) {
			mkdir($packagesFolder, 0755, true);
		}

		$errors = false;
		foreach ($versionList as $version => &$config) {

			$url = sprintf(self::BASE_REPO_TEMPLATE, $config['path'] . '/lib/core/Tiki/Package/ComposerPackages.yml');
			if (! $ymlFile = @file_get_contents($url)) {
				$errors = true;
				break;
			}
			$yaml = Yaml::parse($ymlFile);
			$packagesCacheMap = [];

			foreach ($yaml as $packageName => $packageConfig) {
				$output->writeln("=========== " . $version . ": " . $packageName . " =====================");
				$output->writeln(date('c'));
				$zipName = $packagePacker->pack($packageName, $packageConfig, $packagesFolder, $tempFolder, $packagesCacheMap);
				$config['packages'][$packageName] = array_merge($packageConfig, ['zipFile' => $zipName]);
				$output->writeln($packagePacker->getOutput());
			}
		}

		if (! $errors) {
			$pageGenerator = new GeneratePages();
			$pageGenerator->generate($versionList, $publicFolder);
		} else {
			trigger_error('Failed to open stream from ' . $url);
			exit(1);
		}
	}
}
